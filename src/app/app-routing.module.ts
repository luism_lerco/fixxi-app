import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { CompletarRegistroComponent } from './components/completar-registro/completar-registro.component';
import { InicioComponent } from './components/dashboard/inicio/inicio.component';
import { MisDatosComponent } from './components/dashboard/mis-datos/mis-datos.component';
import { ContratosComponent } from './components/dashboard/contratos/contratos.component';
import { DatosBancariosComponent} from './components/dashboard/datos-bancarios/datos-bancarios.component';
import { Page404Component } from './components/page404/page404.component';

const routes: Routes = [
  {path:  '', redirectTo: 'signup'  , pathMatch:'full'},
  {path:  'signup', component:SignupComponent},
  {path:  'login',  component:LoginComponent},
  {path:  'completar-registro', component:CompletarRegistroComponent},
  {path:  'dashboard/inicio', component:InicioComponent},
  {path:  'dashboard/mis-datos', component:MisDatosComponent},
  {path:  'dashboard/contratos', component:ContratosComponent},
  {path:  'dashboard/datos-bancarios', component:DatosBancariosComponent},
  {path:  '**', component:Page404Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
