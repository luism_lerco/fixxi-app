import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { InicioComponent } from './components/dashboard/inicio/inicio.component';
import { MisDatosComponent } from './components/dashboard/mis-datos/mis-datos.component';
import { CompletarRegistroComponent } from './components/completar-registro/completar-registro.component';
import { DatosBancariosComponent } from './components/dashboard/datos-bancarios/datos-bancarios.component';
import { ContratosComponent } from './components/dashboard/contratos/contratos.component';

import { Page404Component } from './components/page404/page404.component';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

// Lib PDF
// Import pdfmake-wrapper and the fonts to use
import { PdfMakeWrapper } from 'pdfmake-wrapper';
import * as pdfFonts from "pdfmake/build/vfs_fonts"; // fonts provided for pdfmake

import { NgSelectModule } from "@ng-select/ng-select";

// Set the fonts to use
PdfMakeWrapper.setFonts(pdfFonts);


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    InicioComponent,
    MisDatosComponent,
    CompletarRegistroComponent,
    Page404Component,
    ContratosComponent,
    DatosBancariosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule, 
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    NgSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
