import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { signupInterface } from 'src/app/modelos/signup.interface';
import { debounceTime} from 'rxjs/operators';

import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpform!: FormGroup;

  registerForm = new FormGroup({
    nombre : new FormControl('', Validators.required),
    apellidos : new FormControl('', Validators.required),
    telefono_movil : new FormControl('', Validators.required),
    email : new FormControl('', Validators.email),
    password : new FormControl('', Validators.required),
  })

  constructor(private api:ApiService, private router:Router,private alertas:AlertasService, private formBuilder: FormBuilder) { 
    this.buildForm();
  }

  ngOnInit(): void {
  }

  private buildForm() {
    this.signUpform = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      telefono_movil: ['', [Validators.maxLength(10)]],
      email: ['', Validators.compose([Validators.email,Validators.required])],
      password: ['', [Validators.required]],
      TCAccepted: ['', [Validators.requiredTrue]]
    });

    /*this.signUpform.valueChanges
    .pipe(
      debounceTime(500)
    )
    .subscribe(value => {
      console.log(value);
    });*/
  }

  onSignup(form:signupInterface){
    console.log(form);
    this.api.signupUser(form).subscribe( data =>{
      console.log(data);
      let dataResponse:ResponseInterface = data;
      if (dataResponse.code == "200") {
        localStorage.setItem("token", dataResponse.data.token);
        // Guardamos el ID del usuario para las demas operaciones
        localStorage.setItem("user_id", dataResponse.data.id);
        //let usr_id = localStorage.getItem('user_id');
        this.alertas.showSuccess('Registrado correctamente' , 'Hecho');
        //this.router.navigate(['completar-registro']);
        this.router.navigate(['dashboard/mis-datos']);
      }
      if (dataResponse.code == "400") {
        this.alertas.showError(dataResponse.data, 'Error');
      }
    })

  }

}
