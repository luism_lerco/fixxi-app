import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { LoginInterface } from 'src/app/modelos/login.interface';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    telefono_movil : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required)
  })

  constructor( private api:ApiService, private router:Router, private alertas:AlertasService ) { }

  ngOnInit(): void {

    //Checamos si existe el token
    this.checkLocalStorage();

  }

  checkLocalStorage(){
    //si regresamos a la pagina de login y el token existe entonces redirigimos al usuario al inicio del dash
    if (localStorage.getItem('token')) {
      this.router.navigate(['dashboard/inicio']);
    }
  }

  onLogin(form:LoginInterface){
    console.log(form);
    //Mediante el metodo de la API mandamos la info del formulario de login
    this.api.loginByEmail(form).subscribe(data =>{
      console.log(data);
      let dataResponse:ResponseInterface = data;
      // si es código 200, todo salio bien
      if (dataResponse.code == "200") {
        // Guardamos el token para las demas operaciones
        localStorage.setItem("token", dataResponse.data.token);
        // Guardamos el ID del usuario para las demas operaciones
        localStorage.setItem("user_id", dataResponse.data.id);
        this.alertas.showSuccess('Acceso Correcto', 'Bienvenido');
        //redirigimos al inicio del dash
        this.router.navigate(['dashboard/inicio']); 
      }
      if (dataResponse.code == "400") {
        this.alertas.showError(dataResponse.message, 'Error');
      }
    })
  }

}
