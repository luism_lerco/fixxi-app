import { Component, OnInit } from '@angular/core';

import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { UsuarioInterface } from 'src/app/modelos/usuario.interface';
import { CategoriasInterface } from 'src/app/modelos/categorias.interface';

import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

//Librerias para crear pdf
import { PdfMakeWrapper, Txt, QR, Stack, Table, Img } from 'pdfmake-wrapper';


@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.css']
})
export class ContratosComponent implements OnInit {

  //Inicializamos nuestro arreglo para las peticiones
  public usr_token = {
    token: ''
  }

  public selectedCat: any[] = [];

  public allCat: any[] = [];

  public stringCat: any[] = [];

  public previsualizacion: string = '';

  //Declaramos las interfaces a usar
  datosUsuario!:UsuarioInterface;
  

  subirContrato = new FormGroup({
    token : new FormControl(''),
    contrato: new FormControl('')
  });

  constructor(private api:ApiService, private alertas:AlertasService, private router:Router, private sanitizer: DomSanitizer) { }

  //Variables usadas para generar los datos del contrato
  nombre_usuario    = '';
  apellido_usuario  = '';
  rfc_user          = '';
  curp_user         = '';
  direccion_user    = '';
  StringCat         = '';
  nombContrato      = '';

  //Obetenemos la fecha de hoy para el contrato
  fecha = new Date();
  fechadehoy = this.fecha.toLocaleDateString('es-ES', { year: 'numeric', month: 'long', day: '2-digit' })

  ngOnInit(): void {

    //este arreglo muestra las categorias
    this.api.getCategorias().subscribe(data=>{
      let dataResponse:ResponseInterface = data;
      //console.log(dataResponse);
      this.allCat = Array.from(dataResponse.categorias);
      //console.log(this.allCat);
    })

    this.checkLocalStorage();
    let toke = localStorage.getItem('token');
    //console.log(toke);
    this.usr_token.token = toke;
    this.api.getUsuario(this.usr_token).subscribe(data =>{
      let dataResponse:ResponseInterface = data;
      this.datosUsuario = dataResponse.data;
      console.log(dataResponse.data);
      this.nombre_usuario = this.datosUsuario.nombre;
      this.apellido_usuario = this.datosUsuario.apellidos;
      this.rfc_user = this.datosUsuario.rfc;
      this.curp_user = this.datosUsuario.curp;
      this.direccion_user = this.datosUsuario.direccion;

      //Sacamos las categorias y las pasamos a un arreglo para mostrarlas
      this.selectedCat = Array.from(dataResponse.data.categorias);
      //console.log(this.selectedCat);

      //Con el id de la o las categorias buscamos en el listado el nombre de la categoria
      for(let i=0; i<this.allCat.length; i++){
        for (let j = 0; j < this.selectedCat.length; j++) {
          //buscamos el o los id de las categorias en el listado principal
          if(this.allCat[i].id == this.selectedCat[j]){
            //cuando el id conincida metemos el nombre de la categoria a otro arreglo
            this.stringCat.push(this.allCat[i].name);
          }
        }
      }
      //convertimos los nombres de las categorias a string
      this.StringCat = this.stringCat.toString();
      //console.log(this.StringCat); 

      
      this.nombContrato = dataResponse.data.contrato;
      //console.log(this.nombContrato);

    })
    
    this.subirContrato.patchValue({
      'token' : toke
      }
    );
  }

  //si el contrato ya se subio mostramos el boton del contrato
  sihaycontrato(){
    if( this.nombContrato != null){
      return true;
    }
    else{
      return false;
    }
  }

  //checar si estamos logueados
  checkLocalStorage(){
    if (localStorage.getItem('token')) {
      this.router.navigate(['/dashboard/contratos']);
    }
    else{
      this.router.navigate(['login']);
    }
  }

  //cerrar sesion
  onLogout(){
    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    this.router.navigate(['login']);
  }

  //cargamos el archivo
  onFileChange(event:any){

    //console.log(event);
    const file_contract = event.target.files[0];
    //console.log(file_contract);
    this.extraerBase64(file_contract).then((base: any) => {
      //this.previsualizacion = base.base;
      console.log(base);
      this.subirContrato.patchValue({
        contrato: base.base
      });
    }) 

  }

 //Método para convertir a base 64 imagenes o documentos
 extraerBase64 = async ($event: any) => new Promise((resolve) => {
  try {
    const reader = new FileReader();
    reader.readAsDataURL($event);
    reader.onload = () => {
      resolve({
        base: reader.result
      });
    };
    reader.onerror = error => {
      resolve({
        base: null
      });
    };

  } catch (e) {
    return null;
  }
})

  //metodo para actualizar datos
  onUpdate(form:UsuarioInterface){
    console.log(form);
    this.api.updateContrato(form).subscribe( data =>{
      //console.log(data);
      let dataResponse:ResponseInterface = data;
      if (dataResponse.code == "200") {
        this.alertas.showSuccess('Contrato agregado correctamente','Hecho');
        this.router.navigate(['dashboard/inicio']);
      }
    })
  }

  //Creamos PDf del contrato
  generatePDF(){

    let nom_arch:string = "Contrato_" + this.nombre_usuario + "-" + this.apellido_usuario;

    let nom_completo:string = this.nombre_usuario + " " + this.apellido_usuario;

    let fechaActual = this.fechadehoy;

    let actividades = this.StringCat;

    const pdf = new PdfMakeWrapper();

    //configuraciones del PDF
    pdf.pageSize('A4');
    pdf.pageMargins([ 35, 45, 35, 45 ]);

    pdf.add(
      new Txt('CONTRATO DE COMISIÓN MERCANTIL POR LA PRESTACIÓN DE SERVICIOS DE APOYO TÉCNICO').fontSize(15).bold().alignment('center').end
    );

    pdf.add(
      pdf.ln(2)
    );

    pdf.add(
      new Stack([
        'QUE CELEBRAN, POR UNA PARTE: SERVICIOS CORPORATIVOS GLOFIM S DE RL DE CV, A QUIEN EN ADELANTE SE DENOMINARÁ “EL COMITENTE” Y, POR LA OTRA PARTE:', 
        new Txt(nom_completo).bold().end,
        'A QUIEN EN LO SUCESIVO SE LE DENOMINARÁ “EL ASOCIADO TÉCNICO”; Y DE MANERA CONJUNTA SE LES DENOMINARÁN “LAS PARTES” DE ACUERDO CON LAS SIGUIENTES DECLARACIONES Y CLÁUSULAS:'
      ]).alignment('justify').end
    );

    pdf.add(
      new Txt('DECLARACIONES').fontSize(16).bold().alignment('center').end
    );

    pdf.add(
      new Stack([
        new Txt('I.- DECLARA “EL COMITENTE”:').bold().end,
        pdf.ln(1),
        'I.1 Que es una persona moral con capacidad legal y económica para obligarse en los términos del presente contrato.',
        pdf.ln(1),
        'I.3 Que su Registro Federal de Contribuyentes es: SCG220106ET0',
        pdf.ln(1),
        'I.4 Que señala como su domicilio para todo lo relacionado con este contrato el ubicado en: Av. Ejército Nacional 8389 Local 3, Plaza Nacional 2. Col. Partido Senecu, Ciudad Juárez, Chihuahua ',
        pdf.ln(1),
        'I.5 Continúa declarando “EL COMITENTE” que es su voluntad celebrar el presente contrato a fin de que “EL ASOCIADO TÉCNICO”, proceda a realizar los servicios de apoyo técnico y/o mantenimiento de acuerdo con su especialidad que le encomiende y que le sean asignados por medio de la plataforma de internet y/o móvil: Fixxi App.',
        pdf.ln(1),
        new Txt('II.- DECLARA “EL ASOCIADO TÉCNICO”:').bold().end,
        pdf.ln(1),
        'II.1 Que es una persona física, mayor de edad y con capacidad jurídica para obligarse en los términos del presente contrato y que tiene como actividad habitual entre otras la realización de las siguientes actividades: ',
        new Txt(actividades).bold().end,
        'por lo que, posee la suficiente capacidad y experiencia profesional para obligarse en los términos del presente contrato.',
        pdf.ln(1),
        'II.2 Que su Registro Federal de Contribuyentes es:',
        new Txt(this.rfc_user).bold().end,
        pdf.ln(1), 
        'II.3 Que señala como su domicilio para todo lo relacionado con este contrato el ubicado en: ',
        new Txt(this.direccion_user).bold().end,
        pdf.ln(1),
        'Expuesto lo anterior, las partes otorgan las siguientes:',
        pdf.ln(3)
      ]).alignment('justify').end
    );
    pdf.add(
      new Txt('C L Á U S U L A S:').fontSize(14).bold().alignment('center').end
    );
    pdf.add(
      new Stack([
        pdf.ln(1),
        new Txt('PRIMERA: “EL COMITENTE” y “EL ASOCIADO TÉCNICO” celebran el presente contrato mercantil libres de cualquier vicio del consentimiento: error, dolo o mala fe, en la forma que a sus intereses conviene, de acuerdo con la legislación aplicable.').alignment('justify').end,
        pdf.ln(1),
        new Txt('SEGUNDA: “EL COMITENTE” encomienda a “EL ASOCIADO TÉCNICO” la realización de los servicios técnicos y/o de mantenimiento consistentes en: ').end,
        new Txt(actividades).bold().end, 
        new Txt('mismos que son contratados por usuarios por medio de la plataforma de servicios digitales denominada: Fixxi en adelante denominada “La app web” y de la cual es propietario “EL COMITENTE”.').alignment('justify').end,
        pdf.ln(1),
        new Txt ('TERCERA: El precio por los servicios desarrollados por “EL ASOCIADO TÉCNICO” y que serán cubiertos por el usuario de sus servicios se cotizarán de acuerdo a las actividades que desempeñe y deberá ser la cantidad que le indique “EL COMITENTE” por medio de “La app web”.').end,
        pdf.ln(1),
        new Txt ('CUARTA: “EL COMITENTE” y “EL ASOCIADO TÉCNICO” convienen en que la comisión que se establecerá como porcentaje de ganancia para “EL COMITENTE” será un 5% por ciento del pago total que arroje “La app web” por concepto del costo por el servicio contratado por el usuario de “La app web”.').end,
        pdf.ln(1),
        new Txt ('Dicho porcentaje de ganancia se retendrá directamente del pago total que realice el usuario por los servicios contratados a través de “La app web”').end,
        pdf.ln(1),
        new Txt ('QUINTA: El pago que recibirá “EL ASOCIADO TÉCNICO” por la realización de sus servicios será la cantidad que resulte de restarle el porcentaje anteriormente mencionado al pago total que realiza el usuario a través de “La app web” y será pagado mediante transferencia bancaria con un pago en una sola exhibición después de la aprobación del cliente y cierre en “La app web” y una vez que se haya recibido el pago total por parte del usuario de “La app web”. El pago puede tomar uno o dos días hábiles si hay trámites administrativos que realizar. ').end,
        pdf.ln(1),
        new Txt('EL ASOCIADO TECNICO es responsable de emitir los comprobantes requeridos por el SAT por los servicios ofrecidos.').end,
        pdf.ln(1),
        new Txt('SEXTA: “EL ASOCIADO TECNICO” Garantiza su compromiso y obligación con los servicios para los cuales le sean contratados y en caso de cancelación se le cobrará una tarifa del 100% adicional a lo que le sea cotizado el servicio para el cual fue contratado. ').end,
        pdf.ln(1),
        new Txt('También garantiza su trabajo por al menos 15 días a partir de la fecha de finalización del servicio o un periodo mayor si así lo considera y acuerda con el cliente. ').end,
        pdf.ln(1),
        new Txt('SÉPTIMA: “EL ASOCIADO TÉCNICO” para la realización de sus actividades, deberá respetar los intereses de “EL COMITENTE”, esto es tomar las debidas precauciones que la naturaleza de su servicio le indique y en el caso de los servicios relacionados con reparaciones en instalaciones de electricidad y/o gas “EL ASOCIADO TÉCNICO” deberá hacer una validación y verificación exhaustiva para asegurar que no hay riesgo después de la instalación y en caso de que se presentara alguno será el completo responsable de la reparación realizada eximiendo de toda responsabilidad a “EL COMITENTE”.').end,
        pdf.ln(1),
        new Txt('OCTAVA: “EL ASOCIADO TÉCNICO” se compromete a guardar secreto y confidencialidad de todos los asuntos, políticas y documentos internos y/o externos de “EL COMITENTE”, a los cuales pudiera tener acceso o de los cuales tuviera conocimiento por la naturaleza de la actividad que se deriva del presente contrato.').end,
        pdf.ln(1),
        new Txt('NOVENA. – “EL COMITENTE” es una persona física independiente, por lo que no se crea relación laboral con “EL ASOCIADO TÉCNICO”, de igual manera este contrato no crea una sociedad u empresa conjunta, y ninguna parte está autorizada a actuar como representante de la otra parte, salvo en las maneras establecidas por este contrato, para esto “EL ASOCIADO TÉCNICO” declara ser responsable sobre su seguro de gastos médicos   Y en caso de algún percance ocurrido dentro de la prestación de sus servicios deslinda de toda responsabilidad a “EL COMITENTE” debido a que éste último solo es un intermediario para la prestación de su servicio por medio de “La app web”. ').end,
        pdf.ln(1),
        new Txt('NOVENA. – La vigencia del presente contrato es por 6 Meses contado a partir de la fecha de firma del presente, por lo que, al expirar o rescindirse este contrato “LAS PARTES” deberán devolver o, a petición de la Parte correspondiente destruir la Información confidencial de la otra parte, así como se deberá renovar el contrato para continuar utilizando la aplicación y previa acreditación que le otorgue “EL COMITENTE”.').end,
        pdf.ln(1),
        new Txt('DÉCIMA. – Este Contrato es el único celebrado entre “LAS PARTES” en relación con el objeto, por tanto, a fin de que tenga efecto legal cualquier modificación a este contrato, deberá constar realizada en forma escrita y firmada por “LAS PARTES”.').end,
        pdf.ln(1),
        new Txt('DÉCIMA PRIMERA: Este contrato es de naturaleza mercantil y las partes se han obligado en la forma y en los términos que a sus intereses conviene sin haber existido lesión, dolo o vicio alguno de consentimiento, y se comprometen al cumplimiento del mismo y en caso de que alguna de las partes incumpla se obligará al pago de daños y perjuicios a la contraparte que cumple.').end,
        pdf.ln(1),
        new Txt('DÉCIMA SEGUNDA: Para los efectos legales a que haya lugar señalan la parte como domicilios:').end,
        pdf.ln(1),
        new Txt('“EL COMITENTE”: Avenida Ejercito Nacional #8389 Plaza Nacional 2, Colonia Partido Senecu, Ciudad Juárez, Chihuahua C.P. 32459 ').end,
        pdf.ln(1),
        new Txt('“EL ASOCIADO TÉCNICO”:').end,
        new Txt(nom_completo).bold().end,
        pdf.ln(1),
        new Txt('DÉCIMA TERCERA: Para la interpretación y cumplimiento del presente contrato las partes se someten a la jurisdicción y competencia de los tribunales de esta Ciudad Juárez, Chihuahua, México, Distrito Judicial Bravos, renunciando a cualquier otro fuero que por su domicilio presente o futuro pudiera corresponderles.').end,
        pdf.ln(1),
        new Txt('Enteradas las partes del alcance legal y contenido del presente contrato único en su tipo, firman por duplicado el presente contrato en Ciudad Juárez, Chihuahua el día: ').end,
        new Txt(fechaActual).bold().end,
        pdf.ln(2),
        new Txt('“EL COMITENTE”').bold().alignment('center').end,
        new Txt('SERVICIOS CORPORATIVOS GLOFIM S DE RL DE CV').bold().alignment('center').end,
        pdf.ln(3),
        new Txt('“EL ASOCIADO TÉCNICO”').alignment('center').end,
        pdf.ln(2),
        new Txt('___________________________').alignment('center').end,
        new Txt(nom_completo).alignment('center').end,
        pdf.ln(2),
        new Txt('________________________             ____________________').alignment('center').end,
        new Txt('TESTIGO (NOMBRE Y FIRMA)	             TESTIGO (NOMBRE Y FIRMA)	').alignment('center').end,
        pdf.ln(1),
      ]).alignment('justify').end
    );





    //Configuramos salida del archivo
    pdf.create().download(nom_arch);

  }

}
