import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { CategoriasInterface } from 'src/app/modelos/categorias.interface';
import { DocumentosInterface } from 'src/app/modelos/documentos.interface';
import { UsuarioInterface } from 'src/app/modelos/usuario.interface';
import { FilesInterface } from 'src/app/modelos/files.interface';


import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-mis-datos',
  templateUrl: './mis-datos.component.html',
  styleUrls: ['./mis-datos.component.css']
})
export class MisDatosComponent implements OnInit {

  //Declaramos el formbuilder
  updateUser!: FormGroup;

  //Inicializamos nuestro arreglo para las peticiones
  public usr_token = {
    token: ''
  }

  //incializamos la variable donde vamos a pasar la url del avatar
  public previsualizacion: string = '';
  //inicializamos la variable con la ruta de la imagen por defecto
  public prev_avatar: string = '/assets/img/avatar.png';
  //selectedCat: number = 0;

  public selectedCat: any[] = [];

  public nombreCat: any[] = [];
  
  public files: any[] = [];

  public referencias: any[] = [];

  public refer:any =[
    {
      nombre:'',
      telefono:''
    },
    {
      nombre:'',
      telefono: ''
    }  
  ]
    
  

  //Declaramos las interfaces a usar
  datosUsuario!:UsuarioInterface;
  documentos!: DocumentosInterface[];
  categorias!: CategoriasInterface[];
  files_recibidos!: FilesInterface[];


  constructor(private api:ApiService, private alertas:AlertasService, private router:Router, private sanitizer: DomSanitizer, private formBuilder: FormBuilder) { 
    this.buildForm();
  }

  //Campo para saber si el usuario esta registrado
  estatus_del_usuario = '';

  ngOnInit(): void {
    //checamos si estamos logueados
    this.checkLocalStorage();

    //este arreglo muestra los documentos
    this.api.getDocumentos().subscribe(data=>{
      let dataresponse:ResponseInterface = data;
      //console.log(dataresponse);
      this.documentos = dataresponse.documentos;
    })
    //este arreglo muestra las categorias
    this.api.getCategorias().subscribe(data=>{
      let dataResponse:ResponseInterface = data;
      //console.log(dataResponse);
      this.categorias = dataResponse.categorias;
    })
    
    let toke = localStorage.getItem('token');
    console.log(toke);
    this.usr_token.token = toke;
    //console.log(this.usr_token)
    //Mandamos a traer los datos del usuario
    this.api.getUsuario(this.usr_token).subscribe(data =>{
      let dataResponse:ResponseInterface = data;
      this.datosUsuario = dataResponse.data;
      console.log(dataResponse.data);
      //Checamos cual es el status del usuario
      this.estatus_del_usuario = dataResponse.data.estatus;
      //Asignamos a nuestra variable de ruta de imagen
      this.prev_avatar = dataResponse.data.profile_picture;
      
      this.updateUser.patchValue({
        'token' : toke,
        'nombre' : this.datosUsuario.nombre,
        'apellidos' : this.datosUsuario.apellidos,
        'email' : this.datosUsuario.email,
        'telefono' :  this.datosUsuario.telefono,
        'fecha_nacimiento': this.datosUsuario.fecha_nacimiento,
        'sexo': this.datosUsuario.sexo,
        'telefono_movil' : this.datosUsuario.telefono_movil,
        'notas' :  this.datosUsuario.notas,
        'rfc' :  this.datosUsuario.rfc,
        'curp' :  this.datosUsuario.curp,
        'direccion' :  this.datosUsuario.direccion,
        'num_ext' :  this.datosUsuario.num_ext,
        'num_int' :  this.datosUsuario.num_int,
        'estado' :  this.datosUsuario.estado,
        'municipio' :  this.datosUsuario.municipio,
        'ciudad': this.datosUsuario.ciudad,
        'codigo_postal' :  this.datosUsuario.codigo_postal,
        'files': [],
        'colonia': this.datosUsuario.colonia,
        //profile_picture: this.datosUsuario.profile_picture,
        'vacunacion': this.datosUsuario.vacunacion,
        'disponible_fuera_horario': this.datosUsuario.disponible_fuera_horario,
        //'categoria' : [],
        //'referencias' : []
      });
      //Comprobamos si el array de documentos elementos y si es asi
      //asigmamos a un array los documentos que tengamos cargados
      if( dataResponse.data.documentos.length != 0){
        this.files_recibidos = dataResponse.data.documentos;
      }
      //Cargamos las referencias que estan guardadas a un arreglo
      this.refer = this.datosUsuario.referencias;
      //con esta funcion asignamos el arreglo de referencias al formulario actual
      this.patch()

      //console.log(dataResponse.data.categorias);
      //Sacamos las categorias y las pasamos a un arreglo para mostrarlas
      this.selectedCat = Array.from(dataResponse.data.categorias);
      this.updateUser.controls.categoria.patchValue(this.selectedCat);
      

      //obetenemos la url de la imagen y la convertimos otravez a base64 para ponerla en la peticion
      this.getBase64FromUrl(this.prev_avatar).then((avatar_nc:any) =>{
        //console.log(avatar_nc);
        this.updateUser.patchValue({
          profile_picture:avatar_nc
        });
      })
      
    })
    
  }

  //Creamos nuestro Formulario
  private buildForm() {
    this.updateUser = this.formBuilder.group({
      token: [''],
      nombre : [''],
      apellidos : [''],
      email : ['', Validators.email],
      telefono_movil : ['', Validators.maxLength(10)],
      fecha_nacimiento : [''],
      sexo : [''],
      notas : ['', Validators.maxLength(200)],
      files: [''],
      rfc : ['', Validators.pattern(/^([A-ZÑ]|\&){4}[0-9]{2}(0[1-9]|1[0-2])([12][0-9]|0[1-9]|3[01])[A-Z0-9]{3}$/)],
      curp : ['', Validators.pattern(/^[A-Z\d]{18}$/)],
      direccion : [''],
      num_ext  : [''],
      num_int  : [''],
      estado : [''],
      municipio : [''],
      codigo_postal : [''],
      colonia : [''],
      ciudad : [''],
      vacunacion : [''],
      disponible_fuera_horario : [''],  
      telefono : ['', Validators.maxLength(10)],
      //referencias: [''],
      referencias: this.formBuilder.array([]),
      categoria : [''],
      profile_picture : ['']
    });
  }

  //leemos los datos de las referencias que vengan en la respuesta del servidor
  patch(){
    const control = <FormArray>this.updateUser.get('referencias');
    this.refer.forEach(x => {
      //de los datos que encuentre los asigna a cada una de los input de las referencias
      control.push(this.patchValues(x.nombre, x.telefono))
    });
  }

  //esta funcion es para asignar los valores a los inputs correspondientes
  patchValues(nombre:string, telefono:string){
    return this.formBuilder.group({
      nombre: [nombre],
      telefono: [telefono]
    })
  }
  

  //checar si estamos logueados
  checkLocalStorage(){
    if (localStorage.getItem('token')) {
      this.router.navigate(['/dashboard/mis-datos']);
    }
    else{
      this.router.navigate(['login']);
    }
  }

  //cerrar sesion
  onLogout(){
    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    this.router.navigate(['login']);
  }

  //Capturar imagen avatar
  onavatarchange(event:any){
    const avatar_file = event.target.files[0];
    this.extraerBase64(avatar_file).then((file_avatar: any) => {
      this.prev_avatar= file_avatar.base;
      //console.log(file_avatar.base);
      //Una vez que la img del avatar ya esta en base64 la pasamos al form 
      this.updateUser.patchValue({
        profile_picture: file_avatar.base
      });
    }) 
    
  }

  //Capturar documentos y colocarla en el array
  onFileChange(event:any, doc_id:any) {
    //convertimos en string el id del documento
    var docu_id = String(doc_id); 
    //agregamos a una constante las variables del archivo
    const file = event.target.files[0];
    //console.log(file); 
    this.extraerBase64(file).then((base: any) => {
      this.previsualizacion = base.base;
      console.log(base);
      this.files.push({
        file    : base.base,
        tipo_id : docu_id
      });
    }) 
    //vamos agregando los documentos al array para mandarlos
    this.updateUser.controls.files.setValue(this.files); 
  }

  //Método para convertir a base 64 imagenes o documentos
  extraerBase64 = async ($event: any) => new Promise((resolve) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          base: null
        });
      };

    } catch (e) {
      return null;
    }
  })

  //Metodo para convertir desde url una imagen
  getBase64FromUrl = async (url:any) => {
    const data = await fetch(url);
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob); 
      reader.onloadend = () => {
        const base64data = reader.result;   
        resolve(base64data);
      }
    });
  }

  //metodo para actualizar datos
  onUpdate(form:UsuarioInterface){

    console.log(form);
    this.api.updateCliente(form).subscribe( data =>{
      //console.log(data);
      let dataResponse:ResponseInterface = data;
      if (dataResponse.code == "200") {
        this.alertas.showSuccess('Registro actualizado','Hecho');
        this.router.navigate(['dashboard/inicio']);
      }
    })

  }

  //Verificamos si el usuario tiene el status de registrado
  checarStatus(status_u:string){
    if (status_u == 'Habilitado') {
      return true;
    }
    else{
      return false;
    }
  }
  

}
