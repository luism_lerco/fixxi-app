import { Component, OnInit } from '@angular/core';

import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { UsuarioInterface } from 'src/app/modelos/usuario.interface';

import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  //Inicializamos nuestro arreglo para las peticiones
  public usr_token = {
    token: ''
  }
  //incializamos la variable donde vamos a pasar la url del avatar
  public previsualizacion: string = '';
  //inicializamos la variable con la ruta de la imagen por defecto
  public prev_avatar: string = '/assets/img/avatar.png';

  //Declaramos las interfaces a usar
  datosUsuario!:UsuarioInterface;

  constructor(private api:ApiService, private router:Router) { }

  nombre_usuario      = '';
  apellido_usuario    = '';
  estatus_del_usuario = '';
  tel_user            = '';
  mail_user           = '';
  county_user         = '';
  city_user           = '';

  ngOnInit(): void {
    this.checkLocalStorage();
    let toke = localStorage.getItem('token');
    //console.log(toke);
    //Pasamos el token a nuestro objeto para hacer la petición
    this.usr_token.token = toke;
    this.api.getUsuario(this.usr_token).subscribe(data =>{
      let dataResponse:ResponseInterface = data;
      this.datosUsuario = dataResponse.data;
      console.log(dataResponse.data);
      //Asignamos a nuestra variable de ruta de imagen
      this.prev_avatar = dataResponse.data.profile_picture;
      this.estatus_del_usuario = this.datosUsuario.estatus;
      this.nombre_usuario = this.datosUsuario.nombre;
      this.apellido_usuario = this.datosUsuario.apellidos;
      this.tel_user = this.datosUsuario.telefono_movil;
      this.mail_user = this.datosUsuario.email;
      this.county_user = this.datosUsuario.municipio;
      this.city_user = this.datosUsuario.ciudad;
    })
  }

  //checar si estamos logueados
  checkLocalStorage(){
    if (localStorage.getItem('token')) {
      this.router.navigate(['/dashboard/inicio']);
    }
    else{
      this.router.navigate(['login']);
    }
  }

  //cerrar sesion
  onLogout(){
    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    this.router.navigate(['login']);
  }

  //Verificamos si el usuario tiene el status de registrado
  checarStatus(status_u:string){
    if (status_u == 'Habilitado') {
      return true;
    }
    else{
      return false;
    }
  }

  
  

}
