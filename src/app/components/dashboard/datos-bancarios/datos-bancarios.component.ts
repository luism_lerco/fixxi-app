import { Component, OnInit } from '@angular/core';

import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { UsuarioInterface } from 'src/app/modelos/usuario.interface';

import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-datos-bancarios',
  templateUrl: './datos-bancarios.component.html',
  styleUrls: ['./datos-bancarios.component.css']
})
export class DatosBancariosComponent implements OnInit {

  updateDataBankform! : FormGroup;
  
  //Inicializamos nuestro arreglo para las peticiones
  public usr_token = {
    token: ''
  }

  //inicializamos la variable con la ruta de la imagen por defecto
  public prev_avatar: string = '/assets/img/avatar.png';

  public selectedCat: any[] = [];



  //Declaramos las interfaces a usar
  datosUsuario!:UsuarioInterface;

  updateDatabank = new FormGroup({
    token : new FormControl(''),
    datos_bancarios: new FormGroup({
      nombre_titular : new FormControl(''),
      cuenta_tarjeta : new FormControl('',Validators.maxLength(18)),
      banco : new FormControl('')
    })
  });

  constructor(private api:ApiService, private alertas:AlertasService, private router:Router, private formBuilder: FormBuilder) { 
    this.buildForm();
  }

  

  ngOnInit(): void {
    this.checkLocalStorage();
    let toke = localStorage.getItem('token');
    //console.log(toke);
    //Pasamos el token a nuestro objeto para hacer la petición
    this.usr_token.token = toke;
    this.api.getUsuario(this.usr_token).subscribe(data => {
      let dataResponse:ResponseInterface = data;
      this.datosUsuario = dataResponse.data;
      console.log(dataResponse.data);
      //Asignamos a nuestra variable de ruta de imagen
      this.prev_avatar = dataResponse.data.profile_picture;
      /*
      this.updateDatabank.setValue({
        'token' : toke,
        'nombre' : this.datosUsuario.nombre,
        'apellidos' : this.datosUsuario.apellidos,
        'email' : this.datosUsuario.email,
        'telefono' :  this.datosUsuario.telefono,
        'fecha_nacimiento': this.datosUsuario.fecha_nacimiento,
        'sexo': this.datosUsuario.sexo,
        'telefono_movil' : this.datosUsuario.telefono_movil,
        'notas' :  this.datosUsuario.notas,
        'rfc' :  this.datosUsuario.rfc,
        'curp' :  this.datosUsuario.curp,
        'direccion' :  this.datosUsuario.direccion,
        'num_ext' :  this.datosUsuario.num_ext,
        'num_int' :  this.datosUsuario.num_int,
        'estado' :  this.datosUsuario.estado,
        'municipio' :  this.datosUsuario.municipio,
        'ciudad': this.datosUsuario.ciudad,
        'codigo_postal' :  this.datosUsuario.codigo_postal,
        'files': [],
        'colonia': this.datosUsuario.colonia,
        'profile_picture': '',
        'vacunacion': this.datosUsuario.vacunacion,
        'disponible_fuera_horario': this.datosUsuario.disponible_fuera_horario,
        'categoria' : [],
        'referencias' : []
      });
      */
      this.updateDataBankform.patchValue({
        'token' : toke,
        'nombre' : this.datosUsuario.nombre,
        'apellidos' : this.datosUsuario.apellidos,
        'email' : this.datosUsuario.email,
        'telefono' :  this.datosUsuario.telefono,
        'fecha_nacimiento': this.datosUsuario.fecha_nacimiento,
        'sexo': this.datosUsuario.sexo,
        'telefono_movil' : this.datosUsuario.telefono_movil,
        'notas' :  this.datosUsuario.notas,
        'rfc' :  this.datosUsuario.rfc,
        'curp' :  this.datosUsuario.curp,
        'direccion' :  this.datosUsuario.direccion,
        'num_ext' :  this.datosUsuario.num_ext,
        'num_int' :  this.datosUsuario.num_int,
        'estado' :  this.datosUsuario.estado,
        'municipio' :  this.datosUsuario.municipio,
        'ciudad': this.datosUsuario.ciudad,
        'codigo_postal' :  this.datosUsuario.codigo_postal,
        'colonia': this.datosUsuario.colonia,
        'vacunacion': this.datosUsuario.vacunacion,
        'disponible_fuera_horario': this.datosUsuario.disponible_fuera_horario,
        'files': [],
        datos_bancarios:{
          nombre_titular: this.datosUsuario.datos_bancarios.nombre_titular,
          cuenta_tarjeta: this.datosUsuario.datos_bancarios.cuenta_tarjeta,
          banco: this.datosUsuario.datos_bancarios.banco
        }
      });

      //console.log(dataResponse.data.categorias);
      //Sacamos las categorias y las pasamos a un arreglo para mostrarlas
      //this.selectedCat = Array.from(dataResponse.data.categorias);
      //this.updateDataBankform.controls.categoria.patchValue(this.selectedCat);
      

      //obetenemos la url de la imagen y la convertimos otravez a base64 para ponerla en la peticion
      this.getBase64FromUrl(this.prev_avatar).then((avatar_nc:any) =>{
        //console.log(avatar_nc);
        this.updateDataBankform.patchValue({
          profile_picture:avatar_nc
        });
      })

    });
  }

  private buildForm() {
    this.updateDataBankform = this.formBuilder.group({
      token: [''],
      datos_bancarios: this.formBuilder.group({
        nombre_titular : [''],
        cuenta_tarjeta : ['',Validators.maxLength(18)],
        banco : ['']
      }) 
    });
  }

  //Metodo para convertir desde url una imagen
  getBase64FromUrl = async (url:any) => {
    const data = await fetch(url);
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob); 
      reader.onloadend = () => {
        const base64data = reader.result;   
        resolve(base64data);
      }
    });
  }

  //checar si estamos logueados
  checkLocalStorage(){
    if (localStorage.getItem('token')) {
      this.router.navigate(['/dashboard/datos-bancarios']);
    }
    else{
      this.router.navigate(['login']);
    }
  }

  //cerrar sesion
  onLogout(){
    localStorage.removeItem('token');
    //localStorage.removeItem('user_id');
    this.router.navigate(['login']);
  }

  //metodo para actualizar datos
  onUpdate(form:UsuarioInterface){
    console.log(form);
    this.api.updateCliente(form).subscribe( data =>{
      //console.log(data);
      let dataResponse:ResponseInterface = data;
      if (dataResponse.code == "200") {
        this.alertas.showSuccess('Datos Agregados','Hecho');
        this.router.navigate(['dashboard/inicio']);
      }
    })
  }

}
