import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { CategoriasInterface } from 'src/app/modelos/categorias.interface';
import { DocumentosInterface } from 'src/app/modelos/documentos.interface';
import { UsuarioInterface } from 'src/app/modelos/usuario.interface';

import { Router } from '@angular/router';

@Component({
  selector: 'app-completar-registro',
  templateUrl: './completar-registro.component.html',
  styleUrls: ['./completar-registro.component.css']
})
export class CompletarRegistroComponent implements OnInit {

  //Declaramos las interfaces a usar
  documentos!: DocumentosInterface[];
  categorias!: CategoriasInterface[]; 

  constructor(private api:ApiService, private alertas:AlertasService, private router:Router) { }

  docusArray = [
    {
      //url_file :'',
      title_original : '',
      type_file : '',
      tipo_id : ''
    }
  ];

  //Armamos nuestro formulario
  editarForm = new FormGroup({
    id : new FormControl(''),
    token : new FormControl(''),
    nombre : new FormControl(''),
    apellidos : new FormControl(''),
    email : new FormControl(''),
    telefono_movil : new FormControl(''),
    fecha_nacimiento : new FormControl(''),
    sexo : new FormControl(''),
    notas : new FormControl(''),

    //files : new FormArray([]),
    files: new FormControl(''),

    rfc : new FormControl(''),
    curp : new FormControl(''),
    direccion : new FormControl(''),
    num_ext  : new FormControl(''),
    num_int  : new FormControl(''),
    estado : new FormControl(''),
    municipio : new FormControl(''),
    codigo_postal : new FormControl(''),
    //colonia : new FormControl(''),
    ciudad : new FormControl(''),
    //vacunacion : new FormControl(''),
    //disponible_fuera_horario : new FormControl(''),  
    telefono : new FormControl(''),
    categoria : new FormControl('')
    
  })

  ngOnInit(): void {
    //este arreglo muestra los documentos
    this.api.getDocumentos().subscribe(data=>{
      let dataresponse:ResponseInterface = data;
      //console.log(dataresponse);
      this.documentos = dataresponse.documentos;
    })
    //este arreglo muestra las categorias
    this.api.getCategorias().subscribe(data=>{
      let dataResponse:ResponseInterface = data;
      //console.log(dataResponse);
      this.categorias = dataResponse.categorias;
    })
  }

  onFileChange(event:any, doc_id:any) {

    //console.log(event.target.files);
    //console.log(doc_id);
    //convertimos en string el id del documento
    var docu_id = String(doc_id); 
    //agregamos a una constante las variables del archivo
    const file = event.target.files[0];

    //agregamos los datos del los archivos a nuestro array
    this.docusArray.push({
      title_original : file.name,
      type_file : file.type,
      tipo_id : docu_id
    });
    
    console.log(this.docusArray); 
  }

  onUpdate(form:UsuarioInterface){
    //Recorremos el array de los documentos y lo metemos al form
  
      console.log(form);
      this.api.updateCliente(form).subscribe( data =>{
        //console.log(data);
        let dataResponse:ResponseInterface = data;
        if (dataResponse.code == "200") {
          this.alertas.showSuccess('Registro actualizado','Hecho');
          this.router.navigate(['dashboard/inicio']);
        }
      })
  }

}
