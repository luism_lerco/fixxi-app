import { Injectable } from '@angular/core';

import { LoginInterface } from 'src/app/modelos/login.interface';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { signupInterface } from 'src/app/modelos/signup.interface';
import { UsuarioInterface } from 'src/app/modelos/usuario.interface';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:string = "https://dashboard.fixxiapp.com/web/v1/proveedor/";
  url2:string = "https://dashboard.fixxiapp.com/web/v1/categoria/";
  url3:string = "https://dashboard.fixxiapp.com/web/v1/esys/get-tipo-documentos";

  constructor(private http:HttpClient) { }

  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  })

  loginByEmail(form:LoginInterface):Observable<ResponseInterface>{
    let direccion = this.url + "login";
    return this.http.post<ResponseInterface>(direccion, form);
  }

  signupUser(form:signupInterface):Observable<ResponseInterface>{
    let direccion = this.url + "register";
    return this.http.post<ResponseInterface>(direccion, form);
  }

  getUsuario(token:Object):Observable<ResponseInterface>{
    //let direccion = this.url + "get-proveedor?id=" + id;
    let direccion = this.url + "get-proveedor";
    return this.http.post<ResponseInterface>(direccion, token);
  }

  getCategorias():Observable<ResponseInterface>{
    let direccion = this.url2 + "get-categorias";
    return this.http.get<ResponseInterface>(direccion);
  }

  getDocumentos():Observable<ResponseInterface>{
    let direccion = this.url3;
    return this.http.get<ResponseInterface>(direccion);
  }

  updateCliente(form:UsuarioInterface):Observable<ResponseInterface>{
    let direccion = this.url + "update";
    return this.http.post<ResponseInterface>(direccion, form);
  }

  updateContrato(form:UsuarioInterface):Observable<ResponseInterface>{
    let direccion = this.url + "contrato";
    return this.http.post<ResponseInterface>(direccion, form);
  }

}
