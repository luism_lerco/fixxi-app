import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AlertasService {

  constructor(private toastr: ToastrService) { }

  showSuccess(texto:any, titulo:any) {
    this.toastr.success(texto, titulo);
  }

  showError(texto:string, titulo:string) {
    this.toastr.error(texto, titulo);
  }

  showHelp(){
    this.toastr.info('Whatsapp: 656 606 9983', '¿Necesitas ayuda?', {
      disableTimeOut: true,
      timeOut: 0,
      tapToDismiss: false,
      positionClass: 'toast-bottom-left'
    })
  }

}
