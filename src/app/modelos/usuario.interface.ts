export interface UsuarioInterface{
    id              : any;
    token           : any;
    fecha_nacimiento: any;
    sexo            : any;
    telefono        : any;  
    notas           : string;
    rfc             : string;
    curp            : string;
    estatus         : string;
    direccion       : string;
    num_int         : string;
    num_ext         : string;
    estado          : string;
    ciudad          : string;
    municipio       : string;
    codigo_postal   : string;
    colonia         : string;
    vacunacion      : any;
    disponible_fuera_horario : any;
    profile_picture : any;
    //files         :Array<DocusI>
    categoria       : any;
    //categoria: Array<CategoriasI>;
    referencias: Array<ReferenciasI>;
    
    nombre          : string;
    apellidos       : string;
    email           : string;
    telefono_movil  : any;
    datos_bancarios : any;
}

/*
export interface DocusI{
    url_file : any;
    title_original : any;
    type_file : any;
    tipo_id : any;
}
export interface CategoriasI{
    id:number;
    name:string;
}
*/
export interface ReferenciasI{
    nombre:any;
    telefono:any;
}